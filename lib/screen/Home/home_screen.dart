// ignore_for_file: file_names, prefer_const_constructors

import 'package:emart_app/consts/consts.dart';
import 'package:emart_app/consts/iconlist.dart';
import 'package:emart_app/widget/components/featurecard.dart';
import 'package:emart_app/widget/components/featured_buttom.dart';
import 'package:get/get.dart';

import '../../widget/home_buttom.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12),
      color: lightGrey,
      width: context.width,
      height: context.height,
      child: SafeArea(
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: 60,
              color: lightGrey,
              child: TextFormField(
                decoration: const InputDecoration(
                    border: InputBorder.none,
                    suffixIcon: Icon(Icons.search, size: 30),
                    filled: true,
                    fillColor: whiteColor,
                    hintText: search,
                    hintStyle: TextStyle(
                      color: textfieldGrey,
                      fontFamily: semibold,
                    )),
              ),
            ),
            10.heightBox,
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                physics: const BouncingScrollPhysics(),
                child: Column(children: [
                  VxSwiper.builder(
                    aspectRatio: 16 / 9,
                    autoPlay: true,
                    height: 200,
                    enlargeCenterPage: true,
                    itemCount: brandlist.length,
                    itemBuilder: (context, index) {
                      return Image.asset(
                        brandlist[index],
                        fit: BoxFit.fill,
                      )
                          .box
                          .rounded
                          .clip(Clip.antiAlias)
                          .margin(const EdgeInsets.symmetric(horizontal: 5))
                          .make();
                    },
                  ),
                  20.heightBox,
                  //deal buttom
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      homeButtoms(
                          height: context.screenHeight * 0.15,
                          width: context.screenWidth / 2.2,
                          icon: icTodaysDeal,
                          title: todayDeal),
                      homeButtoms(
                          height: context.screenHeight * 0.15,
                          width: context.screenWidth / 2.2,
                          icon: icFlashDeal,
                          title: flashsale),
                    ],
                  ),
                  20.heightBox,

                  VxSwiper.builder(
                    aspectRatio: 16 / 9,
                    autoPlay: true,
                    height: 150,
                    enlargeCenterPage: true,
                    itemCount: brandlist.length,
                    itemBuilder: (context, index) {
                      return Image.asset(
                        secondlist[index],
                        fit: BoxFit.fill,
                      )
                          .box
                          .rounded
                          .clip(Clip.antiAlias)
                          .margin(const EdgeInsets.symmetric(horizontal: 5))
                          .make();
                    },
                  ),
                  20.heightBox,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: List.generate(
                      3,
                      (index) => homeButtoms(
                          height: context.screenHeight * 0.15,
                          width: context.screenWidth / 3.5,
                          icon: homebuttonlist[index],
                          title: homebuttontitle[index]),
                    ),
                  ),
                  20.heightBox,
                  Align(
                    alignment: Alignment.centerLeft,
                    child: featureCategories.text
                        .color(darkFontGrey)
                        .size(18)
                        .fontFamily(semibold)
                        .make(),
                  ),
                  10.heightBox,

                  10.heightBox,
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: List.generate(
                          3,
                          (index) => Column(
                                children: [
                                  featuedButton(
                                      icon: featureimg1[index],
                                      title: featureTitle1[index]),
                                  10.heightBox,
                                  featuedButton(
                                      icon: featureimg2[index],
                                      title: featureTitle2[index]),
                                ],
                              )).toList(),
                    ),
                  ),
                  20.heightBox,

                  Container(
                    padding: EdgeInsets.all(12),
                    width: double.infinity,
                    decoration: BoxDecoration(color: redColor),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        featureproduct.text
                            .fontFamily(bold)
                            .color(whiteColor)
                            .size(18)
                            .make(),
                        10.heightBox,
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: List.generate(
                                6,
                                (index) => featuredCard(
                                      image: imgP1,
                                      title: "laptop 4GB/64Gb",
                                      subtitle: "Rs.64000",
                                    )),
                          ),
                        ),
                      ],
                    ),
                  ),
                  20.heightBox,

                  VxSwiper.builder(
                    aspectRatio: 16 / 9,
                    autoPlay: true,
                    height: 200,
                    enlargeCenterPage: true,
                    itemCount: brandlist.length,
                    itemBuilder: (context, index) {
                      return Image.asset(
                        secondlist[index],
                        fit: BoxFit.fill,
                      )
                          .box
                          .rounded
                          .clip(Clip.antiAlias)
                          .margin(const EdgeInsets.symmetric(horizontal: 5))
                          .make();
                    },
                  ),

                  //allproduct Section(),
                  20.heightBox,
                  GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 10,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 8,
                        crossAxisSpacing: 8,
                        mainAxisExtent: 350),
                    itemBuilder: (_, index) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            imgP1,
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          ),
                          Spacer(),
                          "14.1inch Best Business Laptops cheap computer laptop N4020 N4120 Win-dows11 Laptop"
                              .text
                              .maxLines(2)
                              .overflow(TextOverflow.ellipsis)
                              .fontFamily(semibold)
                              .size(16)
                              .color(darkFontGrey)
                              .make(),
                          5.heightBox,
                          "Rs.64000"
                              .text
                              .fontFamily(bold)
                              .size(16)
                              .color(redColor)
                              .make(),
                        ],
                      )
                          .box
                          .roundedSM
                          .white
                          .outerShadowSm
                          .padding(EdgeInsets.all(12))
                          .margin(
                              EdgeInsets.symmetric(horizontal: 4, vertical: 4))
                          .make();
                    },
                  ),
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
