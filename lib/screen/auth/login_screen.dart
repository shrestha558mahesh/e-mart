import 'package:emart_app/consts/consts.dart';
import 'package:emart_app/consts/iconlist.dart';
import 'package:emart_app/screen/auth/signup_screen.dart';
import 'package:emart_app/screen/controller/auth_controller.dart';
import 'package:emart_app/screen/navigationBar/Home.dart';
import 'package:emart_app/widget/applogo.dart';
import 'package:emart_app/widget/bg_widget.dart';
import 'package:emart_app/widget/custom_buttom.dart';
import 'package:emart_app/widget/custom_textfield.dart';
import 'package:get/get.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var controller = Get.put(AuthController());
  var emailcontroller = TextEditingController();
  var passwordcontroller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return bgWidget(
      Scaffold(
        resizeToAvoidBottomInset: false,
        body: Center(
          child: Column(
            children: [
              (context.screenHeight * 0.1).heightBox,
              applogowidget(),
              10.heightBox,
              "Log in to $appname".text.fontFamily(bold).white.size(18).make(),
              10.heightBox,
              Obx(
                () => Column(
                  children: [
                    customTextField(
                        hint: emailhint,
                        title: email,
                        controller: emailcontroller,
                        ispass: false),
                    customTextField(
                        title: password,
                        hint: passwordhint,
                        controller: passwordcontroller,
                        ispass: true),
                    Align(
                      alignment: Alignment.centerRight,
                      child: TextButton(
                        onPressed: () {},
                        child:
                            forgotpass.text.color(Colors.blue).size(14).make(),
                      ),
                    ),
                    5.heightBox,
                    controller.isloading.value
                        ? const CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(redColor),
                          )
                        : customButtom(
                            color: redColor,
                            onpress: () async {
                              controller.isloading(true);
                              await controller
                                  .loginmethod(
                                      context: context,
                                      email: emailcontroller.text,
                                      password: passwordcontroller.text)
                                  .then((value) {
                                if (value != null) {
                                  VxToast.show(context, msg: loginsuccess);
                                  Get.offAll(() => const Home());
                                } else {
                                  controller.isloading(false);
                                }
                              });
                            },
                            textcolor: whiteColor,
                            title: login,
                          ).box.width(context.screenWidth - 50).make(),
                    10.heightBox,
                    createnew.text.color(fontGrey).make(),
                    10.heightBox,
                    customButtom(
                      color: lightGolden,
                      onpress: () {
                        Get.to(() => const SignupScreen());
                      },
                      textcolor: redColor,
                      title: signup,
                    ).box.width(context.screenWidth - 50).make(),
                    10.heightBox,
                    loginwith.text.color(fontGrey).make(),
                    5.heightBox,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        3,
                        (index) => Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CircleAvatar(
                            backgroundColor: lightGrey,
                            radius: 25,
                            child:
                                Image.asset(socialIconList[index], width: 30),
                          ),
                        ),
                      ),
                    )
                  ],
                )
                    .box
                    .white
                    .rounded
                    .padding(const EdgeInsets.all(16))
                    .width(context.screenWidth - 70)
                    .shadowSm
                    .make(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
