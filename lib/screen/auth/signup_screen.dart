import 'package:emart_app/consts/consts.dart';
import 'package:emart_app/screen/auth/login_screen.dart';
import 'package:emart_app/screen/navigationBar/Home.dart';
import 'package:emart_app/screen/controller/auth_controller.dart';
import 'package:emart_app/widget/applogo.dart';
import 'package:emart_app/widget/bg_widget.dart';
import 'package:emart_app/widget/custom_buttom.dart';
import 'package:emart_app/widget/custom_textfield.dart';
import 'package:get/get.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({super.key});

  @override
  State<SignupScreen> createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  bool? ischecked = false;
  var controller = Get.put(AuthController());
  var namecontroller = TextEditingController();
  var emailcontroller = TextEditingController();
  var passwordcontroller = TextEditingController();
  var passwordretrycontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return bgWidget(
      Scaffold(
        resizeToAvoidBottomInset: false,
        body: Center(
          child: Column(
            children: [
              (context.screenHeight * 0.1).heightBox,
              applogowidget(),
              10.heightBox,
              "Join the $appname".text.fontFamily(bold).white.size(18).make(),
              10.heightBox,
              Obx(
                () => Column(
                  children: [
                    customTextField(
                        hint: namehint,
                        title: name,
                        controller: namecontroller,
                        ispass: false),
                    customTextField(
                        hint: emailhint,
                        title: email,
                        controller: emailcontroller,
                        ispass: false),
                    customTextField(
                        title: password,
                        hint: passwordhint,
                        controller: passwordcontroller,
                        ispass: false),
                    customTextField(
                        title: retypepassword,
                        hint: passwordhint,
                        controller: passwordretrycontroller,
                        ispass: false),
                    5.heightBox,
                    Row(
                      children: [
                        Checkbox(
                          activeColor:
                              ischecked == false ? Colors.white : redColor,
                          checkColor: redColor,
                          value: ischecked,
                          onChanged: (newValue) {
                            setState(() {
                              ischecked = newValue;
                            });
                          },
                        ),
                        5.widthBox,
                        Expanded(
                          child: RichText(
                              text: const TextSpan(children: [
                            TextSpan(
                                text: "I agree to the ",
                                style: TextStyle(
                                  fontFamily: bold,
                                  color: fontGrey,
                                )),
                            TextSpan(
                                text: privacypolicy,
                                style: TextStyle(
                                  fontFamily: bold,
                                  color: redColor,
                                )),
                            TextSpan(
                                text: " & ",
                                style: TextStyle(
                                  fontFamily: bold,
                                  color: redColor,
                                )),
                            TextSpan(
                                text: termadncond,
                                style: TextStyle(
                                  fontFamily: bold,
                                  color: redColor,
                                )),
                          ])),
                        ),
                      ],
                    ),
                    10.heightBox,
                    controller.isloading.value
                        ? const CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation(redColor),
                          )
                        : customButtom(
                            color: ischecked == false ? lightGrey : redColor,
                            onpress: () async {
                              if (ischecked != false) {
                                controller.isloading(true);

                                try {
                                  await controller
                                      .signupmethod(
                                          context: context,
                                          email: emailcontroller.text,
                                          password: passwordcontroller.text)
                                      .then((value) {
                                    return controller.storeUserData(
                                        email: emailcontroller.text,
                                        password: passwordcontroller.text,
                                        name: namecontroller.text);
                                  }).then((value) {
                                    if (value != null) {
                                      VxToast.show(context, msg: loginsuccess);
                                      Get.offAll(() => const Home());
                                    } else {
                                      controller.isloading(false);
                                    }
                                  });
                                } catch (e) {
                                  auth.signOut();
                                  VxToast.show(context, msg: e.toString());
                                  controller.isloading(false);
                                }
                              }
                            },
                            textcolor: whiteColor,
                            title: signup,
                          ).box.width(context.screenWidth - 50).make(),
                    10.heightBox,
                    RichText(
                      text: const TextSpan(
                        children: [
                          TextSpan(
                              text: already,
                              style: TextStyle(
                                fontFamily: bold,
                                color: fontGrey,
                              )),
                          TextSpan(
                              text: login,
                              style: TextStyle(
                                fontFamily: bold,
                                color: redColor,
                              )),
                        ],
                      ),
                    ).onTap(() {
                      Get.to(() => const LoginScreen());
                    }),
                    10.heightBox
                  ],
                )
                    .box
                    .white
                    .rounded
                    .padding(const EdgeInsets.all(16))
                    .width(context.screenWidth - 70)
                    .shadowSm
                    .make(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
