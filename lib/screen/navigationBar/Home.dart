// ignore_for_file: file_names, non_constant_identifier_names

import 'package:emart_app/consts/consts.dart';
import 'package:emart_app/screen/Cart/cart_screen.dart';
import 'package:emart_app/screen/Category/category_screen.dart';
import 'package:emart_app/screen/Home/Home_screen.dart';
import 'package:emart_app/screen/account/account_screen.dart';
import 'package:emart_app/screen/controller/homecontroller.dart';
import 'package:get/get.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    var controller = Get.put(HomeController());

    List<BottomNavigationBarItem> NavvarItem = [
      BottomNavigationBarItem(
          icon: Image.asset(icHome, width: 26), label: "Home"),
      BottomNavigationBarItem(
          icon: Image.asset(icCategories, width: 26), label: "Category"),
      BottomNavigationBarItem(
          icon: Image.asset(icCart, width: 26), label: "Cart"),
      BottomNavigationBarItem(
          icon: Image.asset(icProfile, width: 26), label: "Account"),
    ];

    var navbody = [
      const HomeScreen(),
      const CategoryScreen(),
      const CartScreen(),
      const AccountScreen(),
    ];
    return Scaffold(
      body: Column(
        children: [
          Obx(
            () => Expanded(
                child: navbody.elementAt(controller.currentNavIndex.value)),
          ),
        ],
      ),
      bottomNavigationBar: Obx(
        () => BottomNavigationBar(
          currentIndex: controller.currentNavIndex.value,
          selectedItemColor: redColor,
          selectedLabelStyle: const TextStyle(fontFamily: semibold),
          backgroundColor: whiteColor,
          type: BottomNavigationBarType.fixed,
          items: NavvarItem,
          onTap: (value) {
            setState(() {
              controller.currentNavIndex.value = value;
            });
          },
        ),
      ),
    );
  }
}
