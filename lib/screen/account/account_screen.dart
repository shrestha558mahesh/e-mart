import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emart_app/consts/consts.dart';
import 'package:emart_app/screen/account/component/details_card.dart';
import 'package:emart_app/screen/account/edit_profile_screen.dart';
import 'package:emart_app/screen/controller/profile_controller.dart';
import 'package:emart_app/services/firestore_services.dart';
import 'package:emart_app/widget/bg_widget.dart';
import 'package:get/get.dart';

import '../auth/login_screen.dart';
import '../controller/auth_controller.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({super.key});

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  var controller = Get.put(ProfileController());
  @override
  Widget build(BuildContext context) {
    return bgWidget(
      Scaffold(
        body: StreamBuilder(
          stream: FirestoreServices.getuser(currentUser!.uid),
          builder:
              (BuildContext cojntext, AsyncSnapshot<QuerySnapshot> snaphot) {
            if (!snaphot.hasData) {
              return const Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(redColor),
                ),
              );
            } else {
              var data = snaphot.data!.docs[0];

              return SafeArea(
                child: Column(
                  children: [
                    //edit profile nbutton
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: IconButton(
                          onPressed: () {
                            controller.namecontroller.text = data['name'];
                            Get.to(() => EditProfileScreen(
                                  data: data,
                                ));
                          },
                          icon: const Icon(
                            Icons.edit,
                            color: whiteColor,
                          ),
                        ),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          data['imageurl'] == '' &&
                                  controller.profileimgpath.isEmpty
                              ? Image.asset(
                                  imgProfile2,
                                  width: 70,
                                  fit: BoxFit.cover,
                                ).box.roundedFull.clip(Clip.antiAlias).make()
                              : Image.file(
                                  File(controller.profileimgpath.value),
                                  width: 20,
                                  fit: BoxFit.cover,
                                ).box.roundedFull.clip(Clip.antiAlias).make(),
                          10.heightBox,
                          Expanded(
                              child: Column(
                            children: [
                              "${data['name']}"
                                  .text
                                  .white
                                  .fontFamily(semibold)
                                  .size(16)
                                  .make(),
                              "${data['email']}"
                                  .text
                                  .fontFamily(semibold)
                                  .maxLines(2)
                                  .size(16)
                                  .white
                                  .make(),
                            ],
                          )),
                          OutlinedButton(
                                  style: OutlinedButton.styleFrom(
                                      side: const BorderSide(
                                          color: Colors.white)),
                                  onPressed: () async {
                                    await Get.put(AuthController()
                                        .signoutMethod(context));

                                    Get.offAll(() => const LoginScreen());
                                  },
                                  child: "Log Out"
                                      .text
                                      .white
                                      .size(16)
                                      .fontFamily(semibold)
                                      .make())
                              .box
                              .roundedSM
                              .make()
                        ],
                      ),
                    ),
                    20.heightBox,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        detailCard(
                            width: context.screenWidth / 3.4,
                            count: "${data['cart_count']}",
                            title: "in your Cart"),
                        detailCard(
                            width: context.screenWidth / 3.4,
                            count: "${data['wishlist_count']}",
                            title: "in your wishlist"),
                        detailCard(
                            width: context.screenWidth / 3.4,
                            count: "${data['order_count']}",
                            title: "your order"),
                      ],
                    ),

                    40.heightBox,
                    ListView.separated(
                      shrinkWrap: true,
                      separatorBuilder: (_, index) {
                        return const Divider(color: lightGrey);
                      },
                      itemCount: profilebuttonlsit.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          leading: Image.asset(
                            profilebuttonicon[index],
                            width: 22,
                          ),
                          title: profilebuttonlsit[index]
                              .text
                              .fontFamily(semibold)
                              .color(darkFontGrey)
                              .make(),
                        );
                      },
                    )
                        .box
                        .white
                        .rounded
                        .margin(const EdgeInsets.all(12))
                        .padding(const EdgeInsets.symmetric(horizontal: 16))
                        .shadowSm
                        .make()
                        .box
                        .color(redColor)
                        .make()
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
