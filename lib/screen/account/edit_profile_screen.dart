import 'dart:io';

import 'package:emart_app/consts/consts.dart';
import 'package:emart_app/screen/account/account_screen.dart';
import 'package:emart_app/screen/controller/profile_controller.dart';
import 'package:emart_app/widget/bg_widget.dart';
import 'package:emart_app/widget/custom_buttom.dart';
import 'package:emart_app/widget/custom_textfield.dart';
import 'package:get/get.dart';

class EditProfileScreen extends StatelessWidget {
  final dynamic data;
  const EditProfileScreen({super.key, this.data});

  @override
  Widget build(BuildContext context) {
    var controller = Get.find<ProfileController>();

    return bgWidget(Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: Obx(
          () => Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              data['imageurl'] == '' && controller.profileimgpath.isEmpty
                  ? Image.asset(
                      imgProfile2,
                      width: 100,
                      fit: BoxFit.cover,
                    ).box.roundedFull.clip(Clip.antiAlias).make()
                  : Image.file(
                      File(controller.profileimgpath.value),
                      width: 100,
                      fit: BoxFit.cover,
                    ).box.roundedFull.clip(Clip.antiAlias).make(),
              10.heightBox,
              customButtom(
                  color: redColor,
                  textcolor: whiteColor,
                  onpress: () {
                    controller.changeImage(context);
                  },
                  title: "Change"),
              20.heightBox,
              customTextField(
                  controller: controller.namecontroller,
                  hint: "${data['name']}",
                  title: name,
                  ispass: false),
              customTextField(
                  controller: controller.oldpasscontroller,
                  hint: "${data['password']}",
                  title: "Old Password",
                  ispass: true),
              customTextField(
                  controller: controller.newpasscontroller,
                  hint: "${data['password']}",
                  title: "New Password",
                  ispass: true),
              20.heightBox,
              controller.isloading.value
                  ? const CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(redColor),
                    )
                  : SizedBox(
                      width: context.screenWidth - 40,
                      child: customButtom(
                          color: redColor,
                          onpress: () async {
                            controller.isloading(true);
                            if (controller.profileimgpath.value.isNotEmpty) {
                              controller.uploadprofileimg();
                            } else {
                              controller.profileimglink = data['imageurl'];
                            }
                            //old password match database
                            if (data['password'] ==
                                controller.oldpasscontroller.text) {
                              controller.changeAuthpass(
                                  email: data['email'],
                                  password: controller.oldpasscontroller.text,
                                  newpassword:
                                      controller.newpasscontroller.text);
                              controller.updateprofile(
                                  imgurl: controller.profileimglink,
                                  name: controller.namecontroller.text,
                                  password: controller.newpasscontroller.text);
                              VxToast.show(context,
                                  msg: "Password Updated",
                                  bgColor: Colors.green);
                            } else {
                              VxToast.show(context,
                                  msg: "Wrong Old Password",
                                  bgColor: Colors.red);
                              controller.isloading(false);
                            }

                            Get.to(() => const AccountScreen());
                          },
                          textcolor: whiteColor,
                          title: "Save"),
                    ),
              20.heightBox
            ],
          )
              .box
              .white
              .roundedSM
              .shadowSm
              .padding(const EdgeInsets.all(16))
              .margin(const EdgeInsets.only(top: 50, left: 12, right: 12))
              .make(),
        ),
      ),
    ));
  }
}
