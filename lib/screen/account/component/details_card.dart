import 'package:emart_app/consts/consts.dart';

Widget detailCard({width, String? count, String? title}) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      count!.text.size(16).fontFamily(semibold).make(),
      5.heightBox,
      title!.text.color(darkFontGrey).fontFamily(semibold).make(),
    ],
  )
      .box
      .rounded
      .white
      .height(80)
      .width(width)
      .padding(const EdgeInsets.all(4))
      .make();
}
