import 'package:emart_app/consts/consts.dart';
import 'package:emart_app/consts/iconlist.dart';
import 'package:emart_app/screen/Category/category_details.dart';
import 'package:emart_app/widget/bg_widget.dart';
import 'package:get/get.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({super.key});

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return bgWidget(
      Scaffold(
        appBar: AppBar(
          title: categories.text.white.fontFamily(bold).make(),
        ),
        body: Container(
          padding: const EdgeInsets.all(12),
          child: GridView.builder(
            shrinkWrap: true,
            itemCount: 9,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              crossAxisSpacing: 8,
              mainAxisSpacing: 8,
              mainAxisExtent: 200,
            ),
            itemBuilder: (context, index) {
              return Column(children: [
                Image.asset(
                  categoriesimage[index],
                  fit: BoxFit.cover,
                  height: 120,
                  width: 200,
                ).marginAll(5),
                20.heightBox,
                Expanded(
                    child: categorieslist[index]
                        .text
                        .maxLines(2)
                        .ellipsis
                        .size(18)
                        .fontFamily(bold)
                        .color(darkFontGrey)
                        .align(TextAlign.center)
                        .make()),
              ])
                  .box
                  .rounded
                  .white
                  .clip(Clip.antiAlias)
                  .outerShadowSm
                  .make()
                  .onTap(() {
                Get.to(() => CategoryDetails(
                      title: categorieslist[index],
                    ));
              });
            },
          ),
        ),
      ),
    );
  }
}
