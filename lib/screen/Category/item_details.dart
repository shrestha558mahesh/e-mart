import 'package:emart_app/consts/consts.dart';
import 'package:emart_app/consts/iconlist.dart';
import 'package:emart_app/widget/components/featurecard.dart';
import 'package:emart_app/widget/custom_buttom.dart';

class ItemDetails extends StatelessWidget {
  final String? title;
  const ItemDetails({super.key, this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        title: title!.text.color(darkFontGrey).fontFamily(bold).make(),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.share,
                color: darkFontGrey,
              )),
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.favorite_outline,
                color: darkFontGrey,
              )),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 55.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    VxSwiper.builder(
                        autoPlay: true,
                        height: 350,
                        itemCount: 3,
                        aspectRatio: 16 / 9,
                        itemBuilder: (context, index) {
                          return Image.asset(imgFc1,
                              width: double.infinity, fit: BoxFit.fill);
                        }),
                    10.heightBox,
                    title!.text.color(darkFontGrey).fontFamily(bold).make(),
                    10.heightBox,
                    VxRating(
                      onRatingUpdate: (value) {},
                      normalColor: textfieldGrey,
                      selectionColor: golden,
                      count: 5,
                      size: 25,
                      stepInt: true,
                    ),
                    10.heightBox,
                    "\$30,000"
                        .text
                        .color(redColor)
                        .fontFamily(bold)
                        .size(18)
                        .make(),
                    10.heightBox,
                    Row(
                      children: [
                        Expanded(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            "Seller".text.white.fontFamily(semibold).make(),
                            10.heightBox,
                            "In House Brands"
                                .text
                                .color(darkFontGrey)
                                .size(16)
                                .fontFamily(semibold)
                                .make(),
                          ],
                        )),
                        const CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Icon(
                            Icons.message,
                            color: darkFontGrey,
                          ),
                        )
                      ],
                    )
                        .box
                        .height(60)
                        .padding(const EdgeInsets.symmetric(horizontal: 16))
                        .color(textfieldGrey)
                        .make(),
                    Column(
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              width: 100,
                              child: "Color"
                                  .text
                                  .size(16)
                                  .color(textfieldGrey)
                                  .make(),
                            ),
                            Row(
                                children: List.generate(
                              3,
                              (index) => VxBox()
                                  .size(40, 40)
                                  .roundedFull
                                  .color(Vx.randomPrimaryColor)
                                  .margin(
                                      const EdgeInsets.symmetric(horizontal: 4))
                                  .make(),
                            ))
                          ],
                        ).box.padding(const EdgeInsets.all(16)).make(),

                        //quantity Row(),

                        Row(
                          children: [
                            SizedBox(
                              width: 100,
                              child: "Quantity"
                                  .text
                                  .size(16)
                                  .color(textfieldGrey)
                                  .make(),
                            ),
                            Row(children: [
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(Icons.remove)),
                              "0"
                                  .text
                                  .size(16)
                                  .fontFamily(bold)
                                  .color(textfieldGrey)
                                  .make(),
                              IconButton(
                                  onPressed: () {},
                                  icon: const Icon(Icons.add)),
                              "(0 available )".text.color(textfieldGrey).make()
                            ])
                          ],
                        ).box.padding(const EdgeInsets.all(16)).make(),

                        //total
                        Row(
                          children: [
                            SizedBox(
                              width: 100,
                              child: "Total"
                                  .text
                                  .size(16)
                                  .color(textfieldGrey)
                                  .make(),
                            ),
                            Row(children: [
                              "\$0.00"
                                  .text
                                  .size(16)
                                  .color(redColor)
                                  .fontFamily(bold)
                                  .make(),
                            ])
                          ],
                        ).box.padding(const EdgeInsets.all(16)).make(),
                      ],
                    ).box.white.shadowSm.make(),
                    10.heightBox,
                    "Description"
                        .text
                        .color(darkFontGrey)
                        .fontFamily(semibold)
                        .make(),
                    10.heightBox,
                    "This is a dummy data and dummy description"
                        .text
                        .color(darkFontGrey)
                        .fontFamily(semibold)
                        .make(),

                    //button
                    ListView(
                      shrinkWrap: true,
                      children: List.generate(
                        5,
                        (index) => ListTile(
                          title: itemdetailsbutton[index]
                              .text
                              .fontFamily(semibold)
                              .color(darkFontGrey)
                              .make(),
                          trailing: IconButton(
                              onPressed: () {},
                              icon: const Icon(Icons.arrow_forward)),
                        ),
                      ),
                    ),
                    20.heightBox,
                    productyoulike.text
                        .fontFamily(bold)
                        .size(16)
                        .color(darkFontGrey)
                        .make(),
                    10.heightBox,
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: List.generate(
                            6,
                            (index) => featuredCard(
                                  image: imgP1,
                                  title: "laptop 4GB/64Gb",
                                  subtitle: "Rs.64000",
                                )),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              children: [
                SizedBox(
                  width: double.infinity,
                  height: 60,
                  child: customButtom(
                      color: redColor,
                      onpress: () {},
                      textcolor: whiteColor,
                      title: "Add to Cart"),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 60,
                  child: customButtom(
                      color: redColor,
                      onpress: () {},
                      textcolor: whiteColor,
                      title: "Buy"),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
