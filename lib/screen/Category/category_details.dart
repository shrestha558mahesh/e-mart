import 'package:emart_app/consts/consts.dart';
import 'package:emart_app/screen/Category/item_details.dart';
import 'package:emart_app/widget/bg_widget.dart';
import 'package:get/get.dart';

class CategoryDetails extends StatelessWidget {
  final String? title;
  const CategoryDetails({super.key, this.title});

  @override
  Widget build(BuildContext context) {
    return bgWidget(Scaffold(
      appBar: AppBar(
        title: title!.text.fontFamily(bold).white.make(),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.search),
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(12),
        child: Column(
          children: [
            SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              child: Row(
                children: List.generate(
                  6,
                  (index) => "Baby Clothing"
                      .text
                      .size(14)
                      .fontFamily(semibold)
                      .color(darkFontGrey)
                      .makeCentered()
                      .box
                      .white
                      .rounded
                      .size(120, 50)
                      .margin(const EdgeInsets.symmetric(horizontal: 4))
                      .make(),
                ),
              ),
            ),
            20.heightBox,
            Expanded(
                child: Container(
              color: darkFontGrey,
              child: GridView.builder(
                scrollDirection: Axis.vertical,
                physics: const BouncingScrollPhysics(),
                shrinkWrap: true,
                itemCount: 6,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisExtent: 250,
                    mainAxisSpacing: 8,
                    crossAxisSpacing: 8),
                itemBuilder: (context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Image.asset(
                        imgP1,
                        width: 150,
                        height: 150,
                        fit: BoxFit.fill,
                      ),
                      10.heightBox,
                      "14.1inch Best Business Laptops cheap computer laptop N4020 N4120 Win-dows11 Laptop"
                          .text
                          .maxLines(2)
                          .overflow(TextOverflow.ellipsis)
                          .fontFamily(semibold)
                          .size(14)
                          .color(darkFontGrey)
                          .make(),
                      10.heightBox,
                      "Rs.64000"
                          .text
                          .fontFamily(bold)
                          .size(14)
                          .color(redColor)
                          .make(),
                    ],
                  )
                      .box
                      .roundedSM
                      .white
                      .padding(const EdgeInsets.all(4))
                      .margin(
                        const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                      )
                      .make()
                      .onTap(() {
                    Get.to(() => const ItemDetails(
                          title: "Dummy item",
                        ));
                  });
                },
              ),
            ))
          ],
        ),
      ),
    ));
  }
}
