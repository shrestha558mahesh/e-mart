import 'package:emart_app/consts/consts.dart';

const socialIconList = [icFacebookLogo, icGoogleLogo, icTwitterLogo];

const brandlist = [imgSlider1, imgSlider2, imgSlider3, imgSlider4];

const secondlist = [imgSs1, imgSs2, imgSs3, imgSs4];

const featureimg1 = [imgS1, imgS2, imgS3];
const featureimg2 = [imgS4, imgS5, imgS6];

const homebuttonlist = [icTopCategories, icBrands, icTopSeller];
const homebuttontitle = [topcategory, brand, topseller];

const featureTitle1 = [womenDress, girlsDress, girlswatch];
const featureTitle2 = [boysGlasses, mobilephone, tshirt];

const categorieslist = [
  womenclothes,
  menclothes,
  compaccess,
  kidtoy,
  sports,
  jewelery,
  cellphone,
  furniture,
  shofa
];

const categoriesimage = [
  imgFc1,
  imgFc2,
  imgFc3,
  imgFc4,
  imgFc5,
  imgFc6,
  imgFc7,
  imgFc8,
  imgFc9,
  imgFc10,
];
const itemdetailsbutton = [
  video,
  reviews,
  sellerpolicy,
  returnpolicy,
  suppotpolicy,
];

