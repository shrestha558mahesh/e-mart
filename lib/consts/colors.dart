import 'package:flutter/material.dart';

const Color textfieldGrey = Color.fromRGBO(209, 209, 209, 1);
const Color fontGrey = Color.fromRGBO(107, 115, 119, 1);
const Color darkFontGrey = Color.fromRGBO(62, 68, 71, 1);
const Color whiteColor = Color.fromRGBO(249, 249, 249, 1);
const Color lightGrey = Color.fromRGBO(228, 226, 226, 0.961);
const Color redColor = Color.fromRGBO(230, 46, 4, 1);
const Color golden = Color.fromRGBO(255, 168, 0, 1);
const Color lightGolden = Color(0xfffeead1);
