import 'package:emart_app/consts/consts.dart';

const appname = "eMart";
const appversion = "Version 1.0.0";
const credits = "@dr.psychoo";
const email = "E-mail";
const emailhint = "admin@admin.com";
const password = "password";
const passwordhint = "**********";
const forgotpass = "Forgot Password";
const loginsuccess = "Login Successfully";
const login = "Login";
const signup = "Sign up";
const createnew = " or, Create a new account";
const loginwith = "Log in with";

const name = "FullName";
const namehint = "xyz xyz";
const retypepassword = "Conform Password";
const privacypolicy = "Privacy Policy";
const termadncond = "Terms And Conditions";
const already = "Already have an account ?  ";

//homescreen
const search = "Search anything",
    todayDeal = "Today Deal",
    flashsale = "Flash Sale",
    topBrand = "Top Brand",
    topcategory = "Top Categories",
    brand = "Brand",
    topseller = "Top Seller",
    girlswatch = "Girls Watches",
    boysGlasses = "Boys Glasses",
    mobilephone = "Mobile Phone",
    tshirt = "T-shirt",
    girlsDress = "Girls Dresses",
    womenDress = "Women Dresses",
    featureproduct = "Feature Product",
    categories = "Categories",
    featureCategories = "Featured Categories";

//categories
const womenclothes = "Women Clothes",
    menclothes = "Men Clothes",
    compaccess = "Computer & Accessories",
    kidtoy = "kidtoy",
    sports = "Sports",
    jewelery = "jewelery",
    cellphone = "cellphone",
    shofa = "shofa",
    furniture = "Furniture";

const video = "Video",
    reviews = "Reviews",
    sellerpolicy = "Seller Policy",
    returnpolicy = "Return Policy",
    suppotpolicy = "Suppot Policy",
    productyoulike = "Product You May Like";

//profile
const wishlist = "myWishList", orders = "myOrders", message = "Message";
const profilebuttonlsit = [orders, wishlist, message];
const profilebuttonicon = [icOrder, icOrder, icMessages];
