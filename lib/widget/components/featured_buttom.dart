import 'package:emart_app/consts/consts.dart';

Widget featuedButton({String? title, icon}) {
  return Row(
    children: [
      Image.asset(
        icon,
        width: 60,
        fit: BoxFit.fill,
      ),
      10.widthBox,
      title!.text.fontFamily(semibold).color(darkFontGrey).make()
    ],
  )
      .box
      .white
      .width(200)
      .roundedSM
      .margin(const EdgeInsets.symmetric(horizontal: 5, vertical: 3))
      .outerShadowSm
      .make();
}
