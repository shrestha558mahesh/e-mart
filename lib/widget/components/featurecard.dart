import 'package:emart_app/consts/consts.dart';

Widget featuredCard({image, String? title, String? subtitle}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Image.asset(
        image,
        width: 150,
        fit: BoxFit.fill,
      ),
      10.heightBox,
      title!.text.fontFamily(semibold).size(16).color(darkFontGrey).make(),
      10.heightBox,
      subtitle!.text.fontFamily(bold).size(14).color(redColor).make(),
    ],
  )
      .box
      .white
      .roundedSM
      .padding(const EdgeInsets.all(8))
      .margin(const EdgeInsets.symmetric(horizontal: 5))
      .make();
}
