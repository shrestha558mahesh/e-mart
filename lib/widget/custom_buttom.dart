// ignore_for_file: deprecated_member_use

import 'package:emart_app/consts/consts.dart';

Widget customButtom({onpress, color, textcolor, String? title}) {
  return ElevatedButton(
    style: ElevatedButton.styleFrom(
      primary: color,
      padding: const EdgeInsets.all(12),
    ),
    onPressed: onpress,
    child: title!.text.color(textcolor).fontFamily(bold).make(),
  );
}

