import 'package:emart_app/consts/consts.dart';

Widget homeButtoms({height, width, icon, onpess, String? title}) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Center(child: Image.asset(icon, width: 26)),
      5.heightBox,
      Center(
          child: title!.text.fontFamily(semibold).color(darkFontGrey).make()),
    ],
  ).box.rounded.white.size(width, height).make();
}

